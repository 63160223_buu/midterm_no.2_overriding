/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.midterm_no2;

/**
 *
 * @author focus
 */
public class Test {

    public static void main(String[] args) {

        Germany barca1 = new Germany("Ter Stegen", 1, "GoalKeeper", 29); //กำหนดค่าต่างๆใส่ในตัวแปร 
        barca1.FootballClub();// ให้Object barca1 ไปทำสมการในMethod FootballClub  ในClass Germany เพื่อหาผลลัพท์
        barca1.Country();//ให้Object barca1 ไปทำสมการในMethod Country  ในClass Germany เพื่อหาผลลัพท์
        System.out.println(" ");//เว้นช่องว่างเพื่อความสวยงาม งาม งาม งาม งามมมมม

        Spain barca3 = new Spain("Pique", 3, "Defender", 34);
        barca3.FootballClub();
        barca3.Country();
        System.out.println(" ");

        France barca15 = new France("Lenglet", 15, "Defender", 26);
        barca15.FootballClub();
        barca15.Country();
        System.out.println(" ");

        Spain barca18 = new Spain("Jordi Alba", 18, "Left Wing Back", 32);
        barca18.FootballClub();
        barca18.Country();
        System.out.println(" ");

        Spain barca20 = new Spain("Sergio Roberto", 20, "Right Wing Back", 29);
        barca20.FootballClub();
        barca20.Country();
        System.out.println(" ");

        Spain barca5 = new Spain("Busquets", 5, "Defensive Midfielder", 33);
        barca5.FootballClub();
        barca5.Country();
        System.out.println(" ");

        Croatia barca4 = new Croatia("Rakitic", 4, "Midfielder", 33);
        barca4.FootballClub();
        barca4.Country();
        System.out.println(" ");

        Chile barca8 = new Chile("Vidal", 8, "Midfielder", 34);
        barca8.FootballClub();
        barca8.Country();
        System.out.println(" ");

        Brasil barca7 = new Brasil("Coutinho", 7, "Left Wing", 29);
        barca7.FootballClub();
        barca7.Country();
        System.out.println(" ");

        Uruguay barca9 = new Uruguay("Suarez", 9, "Striker", 34);
        barca9.FootballClub();
        barca9.Country();
        System.out.println(" ");

        Argentina barca10 = new Argentina("Messi", 10, "Right Wing", 34);
        barca10.FootballClub();
        barca10.Country();
        System.out.println(" ");
        
    }
}
