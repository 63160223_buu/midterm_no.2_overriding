/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.midterm_no2;

import java.awt.BorderLayout;

/**
 *
 * @author focus
 */
public class BarcelonaPlayer {
    protected String name;
    protected int number;
    protected int age;
    protected String position;
    
    public BarcelonaPlayer(String name, int number,String position, int age){ // สร้างConstructor เพื่อเก็บชื่อ , เบอร์เสื้อ , ตำแหน่ง , อายุ ของนักเตะ
        this.name = name;
        this.number = number;
        this.age = age;
        this.position = position;
    }
    
    public void FootballClub(){ //สร้าง Method เพื่อให้คลาสอื่นๆ extends ไปใช้ต่อ
        System.out.print("Club is : [ Barcelona ] , " + "Number is : [ " + this.number + " ] , "
                + "Name is : [ " + this.name + " ] , Age is : [ "+this.age +" ] , Position is : [ " + this.position + " ]");
    }
    
    public void Country(){//สร้าง Method เพื่อให้คลาสอื่นๆ extends ไปใช้ต่อ
        System.out.println("Country Of Player");
    }
}
