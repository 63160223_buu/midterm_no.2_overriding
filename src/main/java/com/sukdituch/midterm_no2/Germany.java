/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.midterm_no2;

/**
 *
 * @author focus
 */
public class Germany extends BarcelonaPlayer {// สืบทอด คลาสแม่มา

    public Germany(String name, int number, String position, int age) {
        super(name, number, position, age);//นำตัวแปรของคลาสแม่มาใช้
    }

    @Override//ทำการสืบทอด Method FootballClub ของClassแม่มาใช้
    public void FootballClub(){
        System.out.print("Club is : [ Barcelona ] , " + "Number is : [ " + this.number + " ] , "
                + "Name is : [ " + this.name + " ] , Age is : [ "+this.age +" ] , Position is : [ " + this.position + " ]");
    }

    @Override//ทำการสืบทอด Method FootballClub ของClassแม่มาใช้
    public void Country() {
        System.out.println(" , Nationality is : [ German ]");
    }

}
